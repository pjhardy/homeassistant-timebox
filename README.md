# homeassistant-timebox

This is a [Home Assistant](https://home-assistant.io/) custom component to interface with the
[TimeBox](http://www.divoom.com/index.php?s=/Home/Article/detail/id/7.html) pixel speaker. It may also work with the TimeBox Mini (but
I haven't tested it). The AuraBox is not supported.

Home Assistant will connect to the TimeBox via Bluetooth. The component
currently supports switching between modes (clock, temperature), and
can upload and display a local image.

## hass.io

Please note that this component *is not compatible* with
[hass.io](https://www.home-assistant.io/hassio/). This isn't due to issues
with hass.io, but [Bug #7352](https://bugs.alpinelinux.org/issues/7352) in
the upstream Linux distribution hass.io is built on. I'm not in a position
to fix that bug, a workaround would involve significant time and effort,
and I'm not sure if it would be effective in any case. Sorry, but hass.io
support is just not something I'm able to achieve right now.

# Getting started

## Bluetooth interface

I'm using a cheap USB Bluetooth dongle. Any that's supported by your
operating system should be fine. The Bluetooth interface built in
to the Raspberry Pi 3 probably works, but hasn't yet been tested.

## Configure interface

**Short version**: With the TimeBox turned on and looking for a
Bluetooth connection, run the following

```
sudo hciconfig hci0 up
hcitool scan
```

This should eventually find the TimeBox, and print its MAC address.
Keep a note of this address, we'll need it later.

**Long version**: I followed [this guide](https://www.pcsuggest.com/linux-bluetooth-setup-hcitool-bluez/) to get Bluetooth up and
running on my hardware. Your mileage may vary, especially if you're
trying to use the Raspberry Pi built-in interface (contributions to
this guide very welcome!)

## Install the custom component

* Download the repository. If you know git, a clone is fine. If not,
  go to https://bitbucket.org/pjhardy/homeassistant-timebox/downloads/
  and hit the "Download repository" link to get the most recent code
  in a zip file.
* Copy `notify/timebox.py` from this repository to
  `custom_components/notify/timebox.py` in your Home Assistant
  configuration directory.
* Copy `timebox-images` from this repository to your Home Assistant
  configuration directory. This folder is used to store images
  to send and display on the TimeBox.

## Enable the custom component

This custom component adds a new platform to the Notifications
component. It can be enabled by adding this to your `configuration.yaml`:

```yaml
notify:
  - name: NOTIFIER_NAME
    platform: timebox
    mac: YOUR_TIMEBOX_MAC_ADDRESS
    image_dir: timebox-images
```

### Configuration variables:

* name (Optional): The name for the notifier.
* mac (Required): The Bluetooth MAC address for the TimeBox.
* image_dir (Required): A directory, relative to the configuration dir,
  containing image files in JSON format. The component will use these
  to display static images on the TimeBox. See below for creating images.
  
## Usage

This custom component acts as a notify platform. This means that the
Service Data requires a message parameter, even though we're not using
it. Leave the message parameter blank, and specify TimeBox mode and
other information in the data parameter of the Service Data payload.

### Basic display modes

```json
{
  "message": "",
  "data": {
    "mode": "MODE"
  }
}
```

`MODE` can be one of:

* `clock`: Display the built-in TimeBox clock. This mode also accepts
  an optional `color` parameter in the data dictionary. See below
  for how to specify colours.
* `temp`: Display the built-in temperature display. This mode also
  accepts an optional `color` parameter in the data dictionary. See
  below for how to specify colours.
* `off`: Turn the display off.

### Display an image loaded from a file

```json
{
  "message": "",
  "data": {
    "mode": "image-file",
    "file-name": "FILENAME"
  }
}
```

This mode will send `timebox-images/FILENAME.json` to the TimeBox and
display it. This repository includes a few example images, see below
for how to create your own.

### Colours and images

The colour of an individual pixel on the TimeBox is represented by an
array of three integers for the red, green and blue channels. Each
integer is in the range 0-15. White is thus `[15, 15, 15]`. To
display the time in blue, you could send the following in the notify
service call

```json
{
  "message": "",
  "data": {
    "mode": "temp",
    "color": [0,0,15]
  }
}
```

This component represents static images as an 11x11 2D array of these
colour pixels. This repository includes a handful of utility and exmaple
images which can be used as a basis for creating new images. There is
currently no facility for converting or loading real images, but I hope
to eventually load suitably sized images like PNG or GIF directly.

For example, to load the "stormy weather" icon included in
`timebox-images/storm.json` in this repository, use the following in the
service call

```json
{
  "message": "",
  "data": {
    "mode": "image-file",
    "file-name": "storm"
  }
}
```
